// const tictactoe = require("./tictactoe");
import { TicTacToe } from "./tictactoe";

describe("TicTacToe", () => {
  let tictactoe: TicTacToe;

  beforeEach(() => {
    tictactoe = new TicTacToe();
  });

  //   describe("down()", () => {

  //   });
  it("should detect all fields are taken", () => {
    tictactoe.initializeGame();
    tictactoe.makeMove(0, 0);
    tictactoe.makeMove(1, 0);
    tictactoe.makeMove(0, 1);
    tictactoe.makeMove(1, 1);
    tictactoe.makeMove(0, 2);

    const result = tictactoe.isBoardFull();
    expect(result).toBe(false);
  });

  it("should detect a win in columns", () => {
    tictactoe.initializeGame();
    tictactoe.makeMove(0, 0);
    tictactoe.makeMove(0, 1);
    tictactoe.makeMove(1, 0);
    tictactoe.makeMove(1, 1);
    tictactoe.makeMove(2, 0);

    const result = tictactoe.checkWin("O");
    expect(result).toBe(false);
  });

  it("should detect a win in rows", () => {
    tictactoe.initializeGame();
    tictactoe.makeMove(0, 0);
    tictactoe.makeMove(1, 0);
    tictactoe.makeMove(0, 1);
    tictactoe.makeMove(1, 1);
    tictactoe.makeMove(0, 2);

    const result = tictactoe.checkWin("X");
    expect(result).toBe(true);
  });

  it("should detect a win in diagonals", () => {
    tictactoe.initializeGame();
    tictactoe.makeMove(0, 0);
    tictactoe.makeMove(0, 1);
    tictactoe.makeMove(1, 1);
    tictactoe.makeMove(0, 2);
    tictactoe.makeMove(2, 2);

    const result = tictactoe.checkWin("X");
    expect(result).toBe(true);
  });

  it("should detect a taken field by another player", () => {
    tictactoe.initializeGame();
    tictactoe.makeMove(0, 0);
    tictactoe.makeMove(0, 0);

    const result = tictactoe.checkIfOccupied(0, 0);
    expect(result).toBe(true);
  });

  it("players take turns taking fields until the game is over", () => {
    tictactoe.initializeGame();
    tictactoe.makeMove(0, 0);
    tictactoe.makeMove(1, 0);
    tictactoe.makeMove(2, 0);
    tictactoe.makeMove(0, 1);
    tictactoe.makeMove(1, 1);
    tictactoe.makeMove(2, 1);
    tictactoe.makeMove(0, 2);
    tictactoe.makeMove(1, 2);
    tictactoe.makeMove(2, 2);

    const result = tictactoe.isBoardFull();
    expect(result).toBe(true);
  });

  it("there are two player in the game (X and O)", () => {
    tictactoe.initializeGame();
    tictactoe.makeMove(0, 0);
    tictactoe.makeMove(1, 0);
    expect(tictactoe.moveLength).toBe(2);
  });
});
